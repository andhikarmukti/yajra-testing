<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
<script id="details-template" type="text/x-handlebars-template">
    <table class="table">
        <tr>
            <td>Nama:</td>
            <td>@{{nama}}</td>
        </tr>
    </table>
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var template = Handlebars.compile($("#details-template").html());
        var table = $('#table-user').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copy', 'excel', 'pdf', 'csv', 'print'
            ],
            "lengthMenu": [
                [ 10, 25, 50, 100, 1000, -1 ],
                [ '10 rows', '25 rows', '50 rows', '100 rows', '1000 rows', 'All' ]
            ],
            processing: true,
            serverSide: true,
            ajax: "{{ url('/yajra') }}",
            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "searchable":     false,
                    "data":           null,
                    "defaultContent": '<button class="btn btn-xs btn-success">Detail</button>'
                },
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'company.company_name', name: 'company.company_name'},
                {data: 'is_active', name: 'is_active'},
                {data: 'action', name: 'action'},
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });
        // Add event listener for opening and closing details
        $('#table-user tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( template(row.data()) ).show();
                tr.addClass('shown');
            }
        });
    })
</script>
