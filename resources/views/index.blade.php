<!DOCTYPE html>
<html>

<head>
    <title>Yajra Test</title>

    <link rel="stylesheet" type="text/css"
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>DIKA GANTENG</h1>
                <div class="table-responsive">
                    <table class="table table-hover" id="table-user">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Company</th>
                                <th>Status</th>
                                <th>Detail</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js   "></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
    <script id="details-template" type="text/x-handlebars-template">
        <div class="card col-4 p-4">
            <tr>
                <td>Name:</td>
                <td>@{{name}}</td>
            </tr>
            <br>
            <tr>
                <td>Email:</td>
                <td>@{{email}}</td>
            </tr>
            <br>
            <tr>
                <td>Company:</td>
                <td>@{{company.company_name}}</td>
            </tr>
        </div>
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            var template = Handlebars.compile($("#details-template").html());
            var table = $('#table-user').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'excel', 'pdf'
                ],
                "lengthMenu": [
                    [10, 20, 50, 100, 200, 500, -1],
                    ['10', '20', '50', '100', '200', '500', 'All']
                ],
                processing: true,
                serverSide: true,
                ajax: "{{ url('/yajra') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'company.company_name', name: 'company.company_name'},
                    {data: 'is_active', name: 'is_active'},
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "searchable":     false,
                        "data":           null,
                        "defaultContent": '<button class="badge bg-primary border-0">Detail</button>'
                    },
                    {data: 'action', name: 'action'},
                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                    });
                }
            });
            // Add event listener for opening and closing details
                $('#table-user tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child( template(row.data()) ).show();
                    tr.addClass('shown');
                }
            });
        })
    </script>
</body>

</html>
