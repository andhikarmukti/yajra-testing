<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function yajra()
    {
        $users = User::select([
            'users.id',
            'users.name',
            'users.email',
            'users.company_id',
            'users.is_active'
        ])->with(['company']);

        return DataTables::of($users)
            ->editColumn('is_active', function ($data) {
                if ($data->is_active == 1) {
                    return '<p class="text-success">Aktif</p>';
                } else if ($data->is_active == 0) {
                    return '<p class="text-danger">Non Aktif</p>';
                }
            })
            ->addColumn('action', function ($data) {
                $url_edit = url('/user/edit/' . $data->id);
                $url_delete = url('/user/delete/' . $data->id);

                $button = '<a href=' . $url_edit . ' class="badge bg-warning text-decoration-none ms-1">edit</a>';
                $button .= '<a href=' . $url_delete . ' class="badge bg-danger text-decoration-none ms-1">delete</a>';

                return $button;
            })
            ->rawColumns(['is_active', 'action'])->make();
    }

    public function livewire()
    {
        return view('livewire');
    }
}
