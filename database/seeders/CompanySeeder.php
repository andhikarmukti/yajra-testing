<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'company_name' => 'PT. Artapuri Digital Mediatama'
        ]);
        Company::create([
            'company_name' => 'PT. Sampoerna Tbk'
        ]);
        Company::create([
            'company_name' => 'PT. Krakatau Steel'
        ]);
        Company::create([
            'company_name' => 'PT. Samudera Jaya Abadi'
        ]);
    }
}
